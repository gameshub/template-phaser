const config = {
    type: Phaser.AUTO,
    backgroundColor: "000000",
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'phaser-game',
        width: GAME_WIDTH,
        height: GAME_HEIGHT
    },
    scene: [Manager, GameScene, ExploreScene, PlayScene, CreateScene, EndScene]
};

const app = new Phaser.Game(config);