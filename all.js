const gameState = {
    path: [],

    layerBackground: 0,
    layerFootprints: 1,
    layerSprites: 2,
    layerUI: 3,

    start: {x: 0, y: 0},
    end: {x: 0, y: 0},
    locationsData: [],
    checkpointsData: [],
    remainingCheckpoints : [],
    remainingOrderedCheckpoints: [],

    stepCount: 0,
    minmumSteps: 0,

    modalTextContent: "Est-ce que c'est le bon chemin ?"
    
};

const pictures = [];

let scenario = {};

const scenario01 = {
    verif: {
        debut: {
            1: "9;1"
        },
        fin : {
            1: "1;9"
        },
        segments: {
            1: "7;5/7;5",
            2: "5;7/5;7",
            3: "3;5/3;5",
            4: "5;1/5;1"
        }
    },
    translate : {
        fr: "Comme chaque mercredi après-midi, Simon et Michel se voient pour cuisiner. Aujourd’hui, ils décident de confectionner un gâteau aux pommes chez Simon. La liste d’ingrédients en main, ils doivent définir le chemin le plus rapide pour faire leurs commissions. - Commençons par acheter des pommes au marché ! propose Michel.  - Allons ensuite à la boulangerie pour prendre de la farine et du beurre, ajoute Simon. - Oui et puis dirigeons-nous vers le magasin d’alimentation pour acheter un peu de cannelle ! conclut Michel. Et pour finir, passons chez moi récupérer la plaque à gâteaux. Les courses effectuées, les deux garçons se mettent au travail chez Simon. "
    },
    pictures: {
        1: {
            nomFichier: "house",
            x: "495",
            y: "45",
            nom: {
                0: "Maison de Simon"
            }
        },
        2: {
            nomFichier: "marche",
            x: "395",
            y: "245",
            nom: {
                0: "Marché"
            }
        },
        3: {
            nomFichier: "boulangerie",
            x: "295",
            y: "345",
            nom: {
                0: "Boulangerie"
            }
        },
        4: {
            nomFichier: "magasin",
            x: "195",
            y: "245",
            nom: {
                0: "Magasin d'alimentation"
            }
        },
        5: {
            nomFichier: "home96",
            x: "295",
            y: "45",
            nom: {
                0: "Maison de Michel"
            }
        },
        6: {
            nomFichier: "magasin-bonbons",
            x: "295",
            y: "145",
            nom: {
                0: "Magasin de bonbons"
            }
        },
        7: {
            nomFichier: "poste",
            x: "95",
            y: "145",
            nom: {
                0: "Poste"
            }
        }
    }
};

const GAME_HEIGHT = 0.7 * window.innerHeight;
const GAME_WIDTH = (16 / 9) * GAME_HEIGHT;
const SCALE = GAME_HEIGHT / 720;

const CELL_SIZE = 60;

const REPO_LOAD_URL = 'https://gameacademy.ch/assets/piopl/';

/**
 * StepCounter class
 * @extends Phaser.GameObjects.Sprite
 * @constructor
 * @param {Phaser.Scene} scene - The scene that owns this sprite.
 *  
 *  */

class StepCounter extends Phaser.GameObjects.Sprite {
    constructor(scene) {
        super(scene, GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 20, 'step-counter');
        this.scene.add.existing(this);


        this.setOrigin(0.5, 0);
        this.setDepth(3);
        this._scene = scene;
        this._stepCount = 0;

        this.setAlpha(0); 
        this._stepCounterText = this.scene.add.text(GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 100, this._stepCount, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' });
        this._stepCounterText.setOrigin(0.5, 0);
        this._stepCounterText.setAlpha(0);
        this._minSteps = this.calculateMinSteps();
    }
    get coordX() {
        return this._coordX;
    }
    get coordY() {
        return this._coordY;
    }
    get stepCount() {
        return this._stepCount;
    }

    get minSteps() {
        return this._minSteps;
    }


    /**
     * Increments the step counter
     */
    incrementStepCount() {
        this._stepCount++;
        this._stepCounterText.setText(this._stepCount);
    }

    /**
     * Calculates the minimum number of steps to reach all checkpoints
     */
    calculateMinSteps = () => {
        let minSteps = 0;
        minSteps += Math.abs(gameState.start.x - gameState.checkpointsData[0].x) + Math.abs(gameState.start.y - gameState.checkpointsData[0].y);
        for (let i = 0; i < gameState.checkpointsData.length-1; i++) {
            minSteps += Math.abs(gameState.checkpointsData[i].x - gameState.checkpointsData[i+1].x) + Math.abs(gameState.checkpointsData[i].y - gameState.checkpointsData[i+1].y);
        }
        return minSteps;
    }
}/**
 * class Cell
 * @extends Phaser.GameObjects.Sprite
 * @constructor
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the cell
 * @param {number} coordY - The y coordinate of the cell
 */
class Cell extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {       
        super(scene, coordX * CELL_SIZE, coordY * CELL_SIZE, 'cell');
        this.scene.add.existing(this);
        this.setOrigin(0, 0);
        this.setDepth(gameState.layerBackground);
        this.setInteractive();
        this.setAlpha(0.1);
        this.setScale(SCALE);

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;
        this._isSelected = false;

        this._moveButton;
        this._location; // The location that is on this cell, if there is one

        this.on('pointerdown', this.onPointerDown, this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isSelected() {
        return this._isSelected;
    }

    set isSelected(value) {
        this._isSelected = value;
    }

    /**
     * Spawns the move button on the cell at the cells coordinates
     */
    spawnButton() {
        this._moveButton = new MoveButton(this._scene, this._coordX, this._coordY);
    }

    /**
     * Spawns a location on the cell
     * @param {string} name - The name of the location
     * @param {string} fileName - The name of the file of the location
     */
    spawnLocation(name, fileName) {
        this._location = new Location(this._scene, this._coordX, this._coordY, name, fileName);
    }

    /**
     * Spawns the label on the cell
     */
    spawnLabel() {
        const angle = Phaser.Math.Angle.Between(this.x + CELL_SIZE / 2, this.y + CELL_SIZE / 2, CELL_SIZE * 6, CELL_SIZE * 6); // The angle between the cell and the center of the map
        const yOff = Math.sin(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        const xOff = Math.cos(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        this._label = this._scene.add.image(this.x + CELL_SIZE / 2 + xOff, this.y + CELL_SIZE / 2 + yOff, 'label');
        this._label.setDepth(gameState.layerUI);
        this._label.setOrigin(0.5, 0.5);
        this._text = this._scene.add.text(this._label.x, this._label.y, this._location.name, { fontSize: '12px', fill: '#000' });
        this._text.setDepth(gameState.layerUI);
        this._text.setOrigin(0.5, 0.5);

    }

    /**
     * Destroys the move button of the cell
     */
    destroyOwnButton() {
        if (this._moveButton) this._moveButton.destroy();
    }

    /**
     * Destroys the label of the cell
     */
    destroyOwnLabel() {
        if (this._label) this._label.destroy();
        if (this._text) this._text.destroy();
    }

    /**
     * When the cell is clicked, the move button is spawned on it
     * If there is a location on the cell, the label is spawned
     * If the player is moving, nothing happens
     */
    onPointerDown() {
        if(!gameState.player.isMoving)
        {
            this._scene.children.each((child) => {
                if (child instanceof Cell) {
                    child.destroyOwnButton();
                    child.destroyOwnLabel();
                }
            });
            this.spawnButton();
        } else return;

        if (this._location) {
            this.spawnLabel();
        }

        
    }

}class Checkpoint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, number) {
        super(scene, coordX * CELL_SIZE + CELL_SIZE / 2, coordY * CELL_SIZE + CELL_SIZE / 2, 'bullet_list');

        this._scene = scene;
        this._coordX = coordX;
        this._coordY = coordY;
        this._x = coordX * CELL_SIZE + CELL_SIZE / 2;
        this._y = coordY * CELL_SIZE + CELL_SIZE / 2;
        this._number = number;  

        this.scene.add.existing(this);
        this.setScale(0);
        this.setOrigin(0.5, 0.5);

        this._numberText = this.scene.add.text(this._x, this._y, this._number, { fontFamily: 'Arial', fontSize: 32, color: '#FFFFFF' });
        this._numberText.setOrigin(0.5, 0.5);
        this._numberText.setAlpha(0);

        this._scene.add.tween({
            targets: this,
            scaleX: 0.5*SCALE,
            scaleY: 0.5*SCALE,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this._numberText.setAlpha(1);
            }
        });
        
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get number() {
        return this._number;
    }

    validateCheckpoint = () => {
        this._scene.add.tween({
            targets: this,
            scaleX: 0,
            scaleY: 0,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this.setTexture('bullet_list_true');
                this.setScale(2*SCALE);
                this._scene.add.tween({
                    targets: this,
                    scaleX: 0.5*SCALE,
                    scaleY: 0.5*SCALE,
                    duration: 500,
                    ease: 'Back.easeOut'
                });
            }
        });
        this._numberText.destroy();
    }
}class Footprint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, direction) {
        super(scene, coordX * CELL_SIZE, coordY * CELL_SIZE, 'footprints');

        this._coordX = coordX;
        this._coordY = coordY;
        this._direction = direction;

        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setScale(SCALE);
        this.setAlpha(0.7);

        switch (direction) {
            case 0:
                this.setOrigin(0, 0);
                this.setAngle(0);
                break;
            case 1:
                this.setOrigin(0, 1);
                this.setAngle(90);
                break;
            case 2:
                this.setOrigin(1, 1);
                this.setAngle(180);
                break;
            case 3:
                this.setOrigin(1, 0);
                this.setAngle(270);
                break;
        }

        gameState.footprints.add(this);

    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }
}
class InstructionPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y, text) { 

        super(scene, x, y);
        
        this.scene.add.existing(this);
        
        this._scene = scene;
        this._text = text;
        this._x = x;
        this._y = y;

        this.page = scene.add.image(0, 0, 'page_big').setOrigin(0.5, 0.5);
        this.pageText = scene.add.text(this.page.x, this.page.y, this._text, { fontFamily: 'Arial', fontSize: 16, color: '#000000', align: 'left', wordWrap: { width: 330, useAdvancedWrap: true } }).setOrigin(0.5, 0.5);
        // this.buttonClose = scene.add.sprite(0, 0, 'button_close').setOrigin(0.5, 0.5).setScale(0.4);
        // this.buttonClose.setInteractive();

        // this.buttonClose.x = this.page.width / 2 - 40;
        // this.buttonClose.y = - this.page.height / 2 + 40;

        // this.buttonClose.on('pointerdown', () => {
        //     gameState.menuButton = new MenuButton(this._scene, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.5);
        //     this.destroy();
        // });

        // const buttonClose = this.buttonClose;

        this.setSize(this.page.width, this.page.height);
        this.setPosition(this._x, this._y + GAME_HEIGHT);

        this.add(this.page);
        this.add(this.pageText);
        // this.add(this.buttonClose);

        this.setInteractive();
        this.setScale(SCALE);

        this._scene.input.setDraggable(this);

        this._scene.input.on('drag', (pointer, gameObject, dragX, dragY) => {
            gameObject.x = dragX;
            gameObject.y = dragY;
        });

        // this._scene.input.on('dragend', (pointer, gameObject) => {

        //     if (gameObject.x > GAME_WIDTH / 2) {
        //         buttonClose.x = - gameObject.width / 2 + 40;
        //     }
        //     if (gameObject.y >= GAME_HEIGHT / 2) {
        //         buttonClose.y = - gameObject.height / 2 + 40;
        //     }
        //     if (gameObject.x <= GAME_WIDTH / 2) {
        //         buttonClose.x = gameObject.width / 2 - 40;
        //     }
        //     if (gameObject.y < GAME_HEIGHT / 2) {
        //         buttonClose.y = gameObject.height / 2 - 40;
        //     }
        // });

        this.setDepth(6);

        this._scene.add.tween({
            targets: this,
            y: this._y,
            duration: 800,
            ease: 'Quad.easeOut',
            repeat: 0,
            delay: 0
        });
    }
}/**
 * Location class
 * @extends Phaser.GameObjects.Sprite
 * @constructor
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the location
 * @param {number} coordY - The y coordinate of the location
 * @param {string} name - The name of the location
 * @param {string} fileName - The name of the file of the location
 */
class Location extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, name, fileName) { 

        super(scene, coordX * CELL_SIZE, coordY * CELL_SIZE, fileName);

        this._coordX = coordX;
        this._coordY = coordY;  
        this._name = name;
        this._fileName = fileName;

        this.scene.add.existing(this);
        this.setOrigin(0.2, 0.2);
        this.setScale(SCALE);

        gameState.locations.add(this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get name() {
        return this._name;
    }

    getfileName() {
        return this._fileName;
    }

    changeToWhite() {
        this.setTintFill(0xf7f7f7);
    }
}class MenuButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_menu');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8*SCALE);   
        
        this._x = x;
        this._y = y;

        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        //new InstructionPanel(this._scene, this._x, this._y, gameState.instructions);
        this.destroy();
    }
}/**
 * MoveButton class
 * @constructor
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the button
 * @param {number} coordY - The y coordinate of the button
 */

class MoveButton extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene,  coordX * CELL_SIZE, coordY *CELL_SIZE, 'button_move');
        this.scene.add.existing(this);
        this.setOrigin(0.2, 0.2);
        this.setDepth(gameState.layerUI);
        this.setInteractive();

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
        this.setScale(0);
        this.scene.tweens.add({
            targets: this,
            scale: SCALE,
            duration: 100,
            ease: 'Back'
        });
    }

    /**
     * When the button is clicked, the player moves to the cell where the button is located
     * Destroy the button and the labels of the cells
     */
    onPointerDown() {
        gameState.player.moveOne(this._coordX, this._coordY);
        this.destroy();
        this._scene.children.each((child) => {
            if (child instanceof Cell) {
                child.destroyOwnLabel();
            }
        });
    }

}class PlayButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, callback) {
        super(scene, x, y, 'button_play');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this._callback = callback;

        this.on('pointerdown', () => {
            this._callback();
        });

        this.scene.tweens.add({
            targets: this,
            scale: 0.8*SCALE,
            duration: 500,
            ease: 'Back.easeOut'
        });
    }

    onPointerDown() {
        console.log('PlayButton clicked');
    }
}/**
 * Player class
 * @extends Phaser.GameObjects.Sprite
 * @constructor
 * @param {Phaser.Scene} _scene - The scene where the player is
 * @param {number} _coordX - The x coordinate of the player
 * @param {number} _coordY - The y coordinate of the player
 */

class Player extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene, coordX * CELL_SIZE, coordY *CELL_SIZE, 'player');
        this.scene.add.existing(this);
        this.setOrigin(0.2, 0.2);
        this._coordX = coordX;
        this._coordY = coordY;

        this._destinationX;
        this._destinationY;

        this._moveForward = false; // If true, the player will move on the x axis, if false, on the y axis

        this.setDepth(2);

        this._isMoving = false;
        this._scene = scene;
        this.logPos();

        this.setScale(SCALE);   

        this._scene.anims.create({
            key: 'player',
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.anims.play('player', true);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isMoving() {
        return this._isMoving;
    }

    /**
     * @returns {Object} - The position of the player
     */
    getPos() {
        return {x: this._coordX, y: this._coordY};
    }

    /**
     * Moves the player up
     */
    moveUp() {
        if (this._coordY > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 0);
            //this.spawnFootprint(this._coordX, this._coordY, 0);
            this._coordY--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    /**
     * Moves the player down
     */
    moveDown() {
        if (this._coordY < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 2);
            // this.spawnFootprint(this._coordX, this._coordY, 2);
            this._coordY++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player left
     */
    moveLeft() {
        if (this._coordX > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 3);
            // this.spawnFootprint(this._coordX, this._coordY, 3);
            this._coordX--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player right
     */
    moveRight() {
        if (this._coordX < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 1);
            // this.spawnFootprint(this._coordX, this._coordY, 1);
            this._coordX++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    
    /**
     *  Moves the player to a specific position
     * @param {*} x - The x coordinate
     * @param {*} y - The y coordinate
     */
    tweenPosition(x, y) {
        this._isMoving = true;
        this.tween = this.scene.tweens.add({
            targets: this,
            x: x * CELL_SIZE,
            y: y * CELL_SIZE,
            duration: 200,
            ease: 'Power3',
            onComplete: () => {
                this._isMoving = false;
                this.moveOne(this._destinationX, this._destinationY);
            }
        });
    }

    /**
     * Logs the position of the player and checks if the player has reached a checkpoint
     */
    logPos() {
        gameState.path.push({x: this._coordX, y: this._coordY});

        gameState.remainingCheckpoints.forEach((checkpoint) => {
            if (this._coordX === checkpoint.x && this._coordY === checkpoint.y) {
                let index = gameState.remainingCheckpoints.indexOf(checkpoint);
                gameState.remainingCheckpoints.splice(index, 1);
            }
        });

        if(gameState.remainingOrderedCheckpoints.length > 0) {
            if (this._coordX === gameState.remainingOrderedCheckpoints[0].x && this._coordY === gameState.remainingOrderedCheckpoints[0].y) {
                gameState.remainingOrderedCheckpoints.shift();
            }
        }
    }

    /**
     * Spawns footprints on the map when the player moves
     * @param {number} x - The x coordinate of the footprint
     * @param {number} y - The y coordinate of the footprint
     * @param {number} direction - The direction of the footprint, 0 for up, 1 for right, 2 for down, 3 for left
     */
    spawnFootprint(x, y, direction) {
        const footprint = this._scene.add.image(x * CELL_SIZE, y * CELL_SIZE, 'footprints');
        switch (direction) {
            case 0:
                footprint.setOrigin(0, 0);
                footprint.setAngle(0);
                break;
            case 1:
                footprint.setOrigin(0, 1);
                footprint.setAngle(90);
                break;
            case 2:
                footprint.setOrigin(1, 1);
                footprint.setAngle(180);
                break;
            case 3:
                footprint.setOrigin(1, 0);
                footprint.setAngle(270);
                break;
        }
        footprint.setAlpha(0.5);
        footprint.setDepth(gameState.layerFootprints);
        footprint.setScale(SCALE);
        
        gameState.footprints.add(footprint);
    }

    /**
     * @returns {boolean} - True if the player is on the border of the map, false otherwise
     */
    isOnBorder() {
        if (this._coordX === 0 || this._coordX === 15 || this._coordY === 0 || this._coordY === 15) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Moves one step towards the destination
     * @param {number} x - The x coordinate of the destination
     * @param {number} y - The y coordinate of the destination
     */
    moveOne(x,y) {

        this._destinationX = x;
        this._destinationY = y;

        if(this._moveForward) {
            if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                } else if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                }
        } else {
            if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                } else if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                }
        }

        this._moveForward = !this._moveForward;

    }

}   class ResetButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_return');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this.on('pointerdown', this.onPointerDown, this);

        this.scene.tweens.add({
            targets: this,
            scale: 0.8*SCALE,
            duration: 500,
            ease: 'Back.easeOut'
        });
    }

    onPointerDown() {
        const manager = this._scene.scene.get('Manager');
        manager.loadExerciseAndStartScene('PlayScene');
    }

}class ValidateButton extends Phaser.GameObjects.Sprite {
    constructor(scene, validateCallback) {
        super(scene, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.9, 'button_validate');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8*SCALE);    

        this._scene = scene;
        this._validateCallback = validateCallback;

        this._x = GAME_WIDTH * 0.9;
        this._y = GAME_HEIGHT * 0.9;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        this._modal = new ValidatePanel(this._scene, this._validateCallback);
        this.destroy();
    }

}class ValidatePanel extends Phaser.GameObjects.Container {
    constructor(scene, validateCallback) {
        super(scene, GAME_WIDTH / 2, GAME_HEIGHT / 2);
        scene.add.existing(this);

        this._scene = scene;
        this._validateCallback = validateCallback;
        this.setDepth(10);

        this.blackRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.7).setOrigin(0.5, 0.5);
        this.blackRectangle.setInteractive();

        this.modal = scene.add.image(0, 0, 'modal').setOrigin(0.5, 0.5);
        this.modalText = scene.add.text(0, - 10 , gameState.modalTextContent, { fontFamily: 'Roboto', fontSize: 24, color: '#000000', align: 'center', wordWrap: { width: 400, useAdvancedWrap: true } }).setOrigin(0.5, 0.5);
        
        this.buttonValidate = scene.add.sprite(this.modal.x + 100, this.modal.y + this.modal.height / 2, 'button_validate').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonValidate.on('pointerdown', () => {
            this._validateCallback();
            this.closePanel(false);
        });

        this.buttonReturn = scene.add.sprite(this.modal.x - 100, this.modal.y + this.modal.height / 2, 'button_return').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonReturn.on('pointerdown', () => {
            this.closePanel(true);
        });

        this.add(this.blackRectangle);
        this.add(this.modal);
        this.add(this.modalText);
        this.add(this.buttonValidate);
        this.add(this.buttonReturn);
 

        this.setScale(0);
        this.openPanel();    
    }

    /**
     * Open the panel with a scale animation
     */
    openPanel() {
        this._scene.tweens.add({
            targets: this,
            scale: 1,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.buttonValidate.setInteractive();
                this.buttonReturn.setInteractive();
            }
        });
    }

    /**
     * Close the panel with a scale animation and create a validate button if needed
     * @param {boolean} mustCreateValidateButton 
     */
    closePanel(mustCreateValidateButton){
        this._scene.tweens.add({
            targets: this,
            scale: 0,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.destroy();
                if(mustCreateValidateButton){
                    new ValidateButton(this._scene, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.5);
                }
                
            }
        });
    }

    validateAnswer() {
        this.buttonValidate.disableInteractive();
        this.buttonReturn.disableInteractive();
        this.faddingRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000).setOrigin(0, 0).setDepth(10).setAlpha(0);
        this._scene.tweens.add({
            targets: this.faddingRectangle,
            alpha: 1,
            duration: 1000,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this._scene.scene.stop(this._scene.scene.key);
                this._scene.scene.start('EndScene');
            }
        });
    }
}// Description: This file is responsible for loading the game mode and the scenario.

class Manager extends Phaser.Scene {

    constructor() {
        super({key: 'Manager', active: true});

        try {
            global.util.callOnGamemodeChange(this.loadGameMode);
        } catch (error) {
            console.log("No game mode is selected." + error);

        }
      }
    
    preload() {

        try {
            this.loadGameMode();
        } catch (error) {
            console.log("Loading select scene because global is not defined " + error);
            this.loadScenario(scenario01);
            this.scene.start('PlayScene');
        }

    }

    create() {
        console.log("Manager scene created.");
    }

    update() {
        
    }

    async fetchedLocalExercise() {
        return fetch(REPO_LOAD_URL + 'village-exercise.json')
        .then(response => response.json())
        .then(data => {
            scenario = data;
        });
    }

    async loadExerciseAndStartScene(sceneName, mapName) {
        let exercise;

        try {
            if(forced){
                exercise = global.resources.getExerciceById(forced);
                exercise = JSON.parse(exercise.exercice);
            } else {
                exercise = global.resources.getExercice();
                exercise = JSON.parse(exercise.exercice);
            }
        } catch {
            console.log("No exercise exists. Test mode. Fetching default scenario.");
            await this.fetchedLocalExercise();
            exercise = scenario;
            exercise.map = mapName;
        }

        this.loadScenario(exercise);
        this.scene.start(sceneName);
    }

    loadScenario = (scenario) => {

        // load the start position
        gameState.start.x = parseInt(scenario.verif.debut[1].split(";")[0]);
        gameState.start.y = parseInt(scenario.verif.debut[1].split(";")[1]);
        
        // load the end position
        gameState.end.x = parseInt(scenario.verif.fin[1].split(";")[0]);
        gameState.end.y = parseInt(scenario.verif.fin[1].split(";")[1]);
        
        // load the locations
        gameState.locationsData = [];
        for(let i in scenario.pictures){
            let child = scenario.pictures[i];
            gameState.locationsData.push(child);
            pictures.push(child.nomFichier);
            i++;
        }

        // load the checkpoints in three different arrays
        gameState.checkpointsData = [];
        gameState.remainingCheckpoints = [];
        for(let i in scenario.verif.segments){
            let child = scenario.verif.segments[i];
            let coord = child.split("/")[0]; 
            let cX = parseInt(coord.split(";")[0]);
            let cY = parseInt(coord.split(";")[1]);
            child = {x: cX, y: cY};
            gameState.checkpointsData.push(child); 
            gameState.remainingCheckpoints.push(child);
            gameState.remainingOrderedCheckpoints.push(child);
            i++;
        }

        gameState.instructions = scenario.translate.fr;
       
    }

    loadGameMode = () => {
        let gameMode = global.util.getGamemode();
        // Stop all scenes except the manager
        this.scene.manager.scenes.forEach(scene => {
            if(scene.scene.key !== 'Manager')
            {
                scene.scene.stop();
            }
        });

        switch(gameMode) {
            case global.Gamemode.Evaluate:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Train:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Explore:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
                
            case global.Gamemode.Create:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
        }
    }

    showTutorialInput = (flag) => { 
        try {
            if(flag) {
                // global.util.showTutorialInputs('btnUpload', 'upload_file');
            } else {
                // global.util.hideTutorialInputs('btnUpload', 'upload_file');
            }
        } catch (error) {
            console.log("Error in showTutorialInput: " + error);
        }
    }

}class GameScene extends Phaser.Scene {

    constructor(key) {
        super(key);
    }

    preload(){

    }

    create(){

    }

    update(){

    }

    createElements = () => {

        this.add.image(0, 0, 'background_wood').setOrigin(0, 0).setScale(SCALE).setDepth(gameState.layerBackground);


        //Create the background
        gameState.bg = this.add.image(0, 0, 'bg_plains').setOrigin(0, 0);
        gameState.bg.setScale(3.75*SCALE);
        gameState.bg.setDepth(gameState.layerBackground);

        //Create the right panel
        gameState.stepCounter = new StepCounter(this);
        gameState.stepCounter.calculateMinSteps();

        //Create the validation Button and the validation function callback
        gameState.validateButton = new ValidateButton(this, () => {
            gameState.resetButton.destroy();
            let index = 0;
            gameState.checkpoints = this.add.group();
            gameState.checkpointsData.forEach(checkpoint => {
                let cp = new Checkpoint(this, checkpoint.x, checkpoint.y, index + 1);
                gameState.checkpoints.add(cp);
                gameState.cells.getChildren().forEach(cell => {
                    cell.disableInteractive();
                });
                index++;
            });
            let delay = 1500;
            gameState.footprints.getChildren().forEach(footprint => {
                delay += 150;
                this.tweens.add({
                    targets: footprint,
                    alpha: 0,
                    duration: 200,
                    delay: delay,
                    ease: 'Back.easeIn',
                    onComplete: () => {
                        gameState.checkpoints.getChildren().forEach(checkpoint => {
                            if(checkpoint.coordX == footprint.coordX && checkpoint.coordY == footprint.coordY) {
                                checkpoint.validateCheckpoint();
                            }
                        });
                    }
                });
            });

            this.time.addEvent({
                delay: delay + 500,
                callback: () => {
                    gameState.playButton = new PlayButton(this, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.9, () => {
                        console.log(gameState.remainingCheckpoints);
                        gameState.playButton.destroy();
                        switch(gameState.remainingCheckpoints.length) {
                            case 0:
                                this.updateStats(2);
                                break;
                            case 1:
                                this.updateStats(1);
                                break;
                            default:
                                this.updateStats(0);
                                break;
                        }
                    });
                }
            });


           
        });
        // gameState.menuButton = new MenuButton(this, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.5);
        gameState.resetButton = new ResetButton(this, GAME_WIDTH * 0.7, GAME_HEIGHT * 0.9);
        gameState.instructionPanel = new InstructionPanel(this, GAME_WIDTH * 0.77, GAME_HEIGHT * 0.4, gameState.instructions);

        //Create the cells
        gameState.cells = this.add.group();
        for (let i = 0; i < 12; i++) {
            for (let j = 0; j < 12; j++) {
                let cell = new Cell(this, i, j);
                gameState.cells.add(cell);
            }
        }

        //Create the player
        gameState.player = new Player(this, gameState.start.x, gameState.start.y);
        gameState.footprints = this.add.group();

        //Create the locations
        gameState.locations = this.add.group();
        gameState.locationsData.forEach(location => {

            //We dot this operation because of the old data model. 
            let coordX = ((parseInt(location.x) + 5) / 50) - 1;
            let coordY = (parseInt(location.y) + 5) / 50;
            let fileName = location.nomFichier;
            let name = location.nom[0];

            //Iterate throught the cells. If one has the same coordinates as the location, spawn the location on it.           
            gameState.cells.getChildren().forEach(cell => {
                if (cell.coordX == coordX && cell.coordY == coordY) {
                    cell.spawnLocation(name, fileName);
                }
            });
        });

        //Create the checkpoints
        gameState.checkpoints = this.add.group();

        // for (let i = 0; i < gameState.checkpointsData.length; i++) {
        //     let name;
        //     gameState.locations.getChildren().forEach(location => {
        //         if(location.coordX == gameState.checkpointsData[i].x && location.coordY == gameState.checkpointsData[i].y) {
        //             name = location.name;
        //         }
        //     new Checkpoint(this, GAME_WIDTH * 0.5, GAME_HEIGHT * 0.2 + 100 * SCALE * i, i, name);
        //     }
        // )};


    }

    displayTextOnPanel = (text) => {
        let textContent = "<p>" + text + "</p>";
        try {
            global.util.setTutorialText(textContent);
        } catch (error) {         
            console.log(error);
        }
    }

    addStats = (mode) => {
        try {
            global.statistics.addStats(mode);
        } catch (error) {
            console.log(error);
        }
    }

    updateStats = (score) => {
        try {
            global.statistics.updateStats(score);
        } catch (error) {
            console.log('Update stats: ' + score);
        }
    }

    updateStatsTimer = (score, seconds) => {
        this.time.addEvent({
            time: seconds * 1000,
            callback: () => {
                this.updateStats(score);
                console.log('Stats updated');
            }
        });
    }
}class CreateScene extends GameScene {
    constructor() {
        super({ key: 'CreateScene' });
    }

    preload() {

    }

    create() {
        console.log("Create scene created.");
    }

    update() {

    }
}class EndScene extends GameScene {
    constructor() {
        super({ key: 'EndScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);
        this.load.spritesheet('playerend', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('checkpoint', 'button_star_on.png');
        this.load.image('checkpoint_off', 'button_star_off.png');
        this.load.image('step-counter', 'step-counter.png');
        this.load.image('modal-end', 'modal.png');
    }

    create() {

        const destinationX = 600;
        const offset = 100;
        this.footprints = 0;

        this.player = this.add.sprite(100, 300, 'playerend');

        this.player.anims.create({
            key: 'playerend',
            frames: this.anims.generateFrameNumbers('playerend', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.player.anims.play('playerend', true);

        this.footprintImage = this.add.image(0, 200, 'step-counter');
        this.footprintText = this.add.text(0, 120, '0', { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.text(destinationX, 400, gameState.stepCounter.minSteps, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.image(destinationX, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 580, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 660, 'checkpoint').setScale(0.5);

        this.add.image(destinationX + offset, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX + offset, 580, 'checkpoint').setScale(0.5);
        
        this.add.image(destinationX + 2 * offset, 500, 'checkpoint').setScale(0.5);


        // const checkpoints = [];
        // for(let i = 0; i < gameState.checkpointsData.length + 1; i++) {
        //     const checkpoint = this.add.image(200  + i * ((destinationX - 200) / gameState.checkpointsData.length), 400, 'checkpoint').setScale(0.5);
        //     checkpoints.push(checkpoint);
        // }

        this.startTimer();

        if(gameState.remainingCheckpoints.length === 0) {
            if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps) {
                console.log('You win! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Bravo! Tu as réussi avec le minimum de pas!');
                this.movePlayerUpdate(destinationX, 2);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.3) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + offset, 1.5);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.5) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 1);
            } else {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Zut! Tu as utilisé trop de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 0.5);
            }
        } else {
            console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
            this.popModal('Zut! Tu as manqué une étape!');
            this.movePlayerUpdate(destinationX - 2 * offset, 0);
        }

    }

    update() {
        this.footprintImage.x = this.player.x;
        this.footprintText.x = this.player.x;
    }

    movePlayerUpdate(destination, score) {
        this.add.tween({
            targets: this.player,
            x: destination,
            duration: 3000,
            ease: 'Linear',
            onComplete: () => {
                this.player.anims.stop();
                this.updateStatsTimer(score, 3);
            }
        });
    }

    startTimer() {
        this.time.addEvent({
            delay: 3000 / gameState.stepCounter.stepCount,
            callback: () => {
                this.footprints++;
                this.footprintText.setText(this.footprints);
                if(this.footprints < gameState.stepCounter.stepCount) {
                    this.startTimer();
                }
            }
        });
    }

    popModal(text) {
        this.modalContainer = this.add.container(0, GAME_HEIGHT);
        this.modal = this.add.image(20, -50, 'modal-end');
        this.modal.setOrigin(0, 1);

        this.textModal = this.add.text(65, -145, text, { fontFamily: 'Arial', fontSize: 22, color: '#000000', align: 'center', wordWrap: { width: 400 } });
        this.textModal.setOrigin(0, 1);
        
        this.modalContainer.add(this.modal);
        this.modalContainer.add(this.textModal);
        this.modalContainer.setScale(0, 0);

        this.add.tween({
            targets: this.modalContainer,
            scaleX: 1,
            scaleY: 1,
            duration: 500,
            ease: 'Bounce',
            delay: 4000
        });
    }
}class ExploreScene extends GameScene {
    constructor() {
        super({ key: 'ExploreScene' });
    }

    preload() {

    }

    create() {
        console.log("Explore scene created.");
    }

    update() {

    }
}class PlayScene extends GameScene {
    constructor() {
        super({ key: 'PlayScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);

        // this.load.image('tiles', 'cosy-free.png');
        // this.load.tilemapTiledJSON('map', 'cosy-12x12.json');
        this.load.image('background_wood', 'background_wood.png');
        this.load.image('bg_plains', 'cosy-12x12.png');
        this.load.image('cell', 'cell60.png');
        this.load.spritesheet('player', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.spritesheet('player_shadow', 'player_shadow_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('footprints', 'footprint.png');
        this.load.image('button_move', 'button_move.png');
        this.load.image('label', 'label.png');
        this.load.image('button_validate', 'button_validate.png');
        this.load.image('button_return', 'button_return.png');
        this.load.image('button_square_off', 'button_square_off.png');    
        this.load.image('modal', 'modal.png');
        this.load.image('page_big', 'page_big.png');
        this.load.image('button_menu', 'button_menu.png');
        this.load.image('button_close', 'button_close.png');
        this.load.image('button_play', 'button_play.png');
        
        this.load.image('bullet_list', 'bullet_list.png');
        this.load.image('bullet_list_false', 'bullet_list_false.png');
        this.load.image('bullet_list_true', 'bullet_list_true.png');

        for (let i = 0; i < pictures.length; i++) {
            this.load.image(pictures[i], pictures[i]+'.png');
        }
        
        this.load.image('step-counter', 'step-counter.png');
    }

    create() {
        console.log("Play scene created.");
        this.createElements();
        this.addStats(2);
        this.displayTextOnPanel(gameState.instructions);
    }

    update() {

    }

}const config = {
    type: Phaser.AUTO,
    width: GAME_WIDTH,
    height: GAME_HEIGHT,
    backgroundColor: "000000",
    parent: 'phaser-game',
    scene: [Manager, GameScene, ExploreScene, PlayScene, CreateScene, EndScene]
};

const app = new Phaser.Game(config);
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
