/**
 * @class ToolsPanel
 * @extends Phaser.GameObjects.Container
 * @description A panel that contains tools for the game
 * @param {Phaser.Scene} scene - The scene that owns this container
 * @param {float} x - x position of the container
 * @param {float} y - y position of the container
 */

class ToolsPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y) {
        super(scene, x, y);
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;

        this._tools = [];

        this._states = {
            CLOSED: 0,
            OPEN: 1,
            ON: 2
        };

        this._state = this._states.CLOSED;
        this._stateOD = this._states.OPEN;
        this._stateSpacing = this._states.OPEN;

        this._buttonOpendyslexic = scene.add.sprite(0, 0, 'button_od')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setAngle(180);

        this._buttonLetterSpacing = scene.add.sprite(0, 0, 'button_spacing')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setAngle(180);

        this._buttonTools = scene.add.sprite(0, 0, 'button_tools')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();
        
        this.add(this._buttonLetterSpacing);
        this.add(this._buttonOpendyslexic);
        this.add(this._buttonTools);

        this._offset = this._buttonTools.width * 0.6;

        this._buttonTools.on('pointerdown', () => {
            this._buttonTools.setTexture('button_tools_pressed');
        });

        // Button Tools on clicked
        this._buttonTools.on('pointerup', () => {
            switch(this._state) {
                case this._states.CLOSED:
                    this._state = this._states.OPEN;
                    this.openPanel();
                    this._buttonTools.setTexture('button_round_close');
                    break;
                case this._states.OPEN:
                    this._state = this._states.CLOSED;
                    this.closePanel();
                    this._buttonTools.setTexture('button_tools');
                    break;
            }
        });


        this._buttonOpendyslexic.on('pointerdown', () => {
            this._buttonOpendyslexic.setTexture('button_od_pressed');
        });

        // Button Open Dyslexic on clicked
        this._buttonOpendyslexic.on('pointerup', () => {
            switch(this._stateOD) {
                case this._states.OPEN:
                    this._stateOD = this._states.ON;
                    this._buttonOpendyslexic.setTexture('button_od_on');
                    break;
                case this._states.ON:
                    this._stateOD = this._states.OPEN;
                    this._buttonOpendyslexic.setTexture('button_od');
                    break;
            }
            this.updateTextStyle(false);
            global.util.openDys();
        });

        this._buttonLetterSpacing.on('pointerdown', () => {
            this._buttonLetterSpacing.setTexture('button_spacing_pressed');
        });

        // Button Letter Spacing on clicked
        this._buttonLetterSpacing.on('pointerup', () => {
            switch(this._stateSpacing) {
                case this._states.OPEN:
                    this._stateSpacing = this._states.ON;
                    this._buttonLetterSpacing.setTexture('button_spacing_on');
                    break;
                case this._states.ON:
                    this._stateSpacing = this._states.OPEN;
                    this._buttonLetterSpacing.setTexture('button_spacing');
                    break;
            }
            this.updateTextStyle(true);
            global.util.spaceLetters();
        });

        this.setDepth(100);
    }

    /**
     * Opens the panel
     */
    openPanel() {
        this.scene.tweens.add({
            targets: this._buttonOpendyslexic,
            x: - this._offset,
            angle: 0,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonOpendyslexic.setInteractive();
            }
        });

        this.scene.tweens.add({
            targets: this._buttonLetterSpacing,
            angle: 0,
            x: - this._offset * 2,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonLetterSpacing.setInteractive();
            }
        });
    }

    /**
     * Closes the panel
     */
    closePanel() {
        this.scene.tweens.add({
            targets: this._buttonOpendyslexic,
            x: 0,
            angle: 180,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonOpendyslexic.disableInteractive();
            }
        });

        this.scene.tweens.add({
            targets: this._buttonLetterSpacing,
            x: 0,
            angle: 180,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonLetterSpacing.disableInteractive();
            }
        });
    }

    /**
     * Updates the text style
     * @param {boolean} isForcingWidth 
     */
    updateTextStyle = (isForcingWidth) => {

        gameState.texts.getChildren().forEach((text) => {

            let defaultWidth = (isForcingWidth) => {
                if (isForcingWidth) {
                    return text.style.wordWrapWidth * 1.6;
                } else {
                    return text.style.wordWrapWidth
                }
            }

            switch(this._stateOD) {
                case this._states.ON:
                    text.setFontFamily('opendyslexic');
                    break;
                case this._states.OPEN:
                    text.setFontFamily('Arial');
                    break;
            }
            
            if(isForcingWidth) {
                switch(this._stateSpacing) {
                    case this._states.ON:
                        text.setLetterSpacing(4);
                        text.setWordWrapWidth(text.style.wordWrapWidth / 1.6);
                        break;
                    case this._states.OPEN:
                        text.setLetterSpacing(0);
                        text.setWordWrapWidth(defaultWidth(isForcingWidth));
                        break;
                }
            }

            text.update();
        });
    }
}