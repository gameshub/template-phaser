class PlayScene extends GameScene {
    constructor() {
        super({ key: 'PlayScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);

        this.load.image('background_wood', 'background_wood.png');
    }

    create() {
        this.createElements();
        this.addStats(2);
    }

    update() {

    }

}