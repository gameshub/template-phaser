class GameScene extends Phaser.Scene {

    constructor(key) {
        super(key);
    }

    preload(){

    }

    create(){

    }

    update(){

    }

    createElements = () => {
        this.add.image(0, 0, 'background_wood').setOrigin(0, 0).setDepth(0);
        console.log('Elements created');
    }

    /**
     * @description Display a text on the panel
     * @param {string} text 
     */
    displayTextOnPanel = (text) => {
        let textContent = "<p>" + text + "</p>";
        try {
            global.util.setTutorialText(textContent);
        } catch (error) {         
            console.log(error);
        }
    }

    /**
     * @description Add stats to the statistics
     * @param {int} mode Mode of the game (0: Create, 1: Play, 2: Evaluate)
     */
    addStats = (mode) => {
        try {
            global.statistics.addStats(mode);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * @description Update the statistics
     * @param {int} score Score to update the statistics
     */
    updateStats = (score) => {
        try {
            global.statistics.updateStats(score);
        } catch (error) {
            console.log('Update stats: ' + score);
        }
    }

    /**
     * @description Update the statistics after a certain amount of time
     * @param {int} score Score to update the statistics
     * @param {float} seconds Seconds to wait before updating the statistics
     */
    updateStatsTimer = (score, seconds) => {
        this.time.addEvent({
            time: seconds * 1000,
            callback: () => {
                this.updateStats(score);
                console.log('Stats updated');
            }
        });
    }
}