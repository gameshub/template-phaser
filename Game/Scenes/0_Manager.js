/**
 * @class Manager
 * @extends Phaser.Scene
 * @description Manager scene is the first scene that is loaded when the game starts. It is responsible for loading the game mode and the scenario.
 */

class Manager extends Phaser.Scene {

    constructor() {
        super({key: 'Manager', active: true});

        try {
            global.util.callOnGamemodeChange(this.loadGameMode);
        } catch (error) {
            console.log("No game mode is selected." + error);

        }
      }
    
    preload() {

        try {
            this.loadGameMode();
        } catch (error) {
            console.log("Loading select scene because global is not defined " + error);
            this.loadScenario(scenario01);
            this.scene.start('PlayScene');
        }

    }

    create() {
        console.log("Manager scene created.");
    }

    update() {
        
    }

    /**
     * @returns {Promise} - A promise that resolves when the local exercise is fetched
     */
    async fetchedLocalExercise() {
        return fetch(REPO_LOAD_URL + 'village-exercise.json')
        .then(response => response.json())
        .then(data => {
            scenario = data;
        });
    }

    /**
     * @description Load the exercise and start the scene
     * @param {string} sceneName - The name of the scene to load
     * @param {string} mapName - The name of the map to load
     */
    async loadExerciseAndStartScene(sceneName, mapName) {
        let exercise;

        try {
            if(forced){
                exercise = global.resources.getExerciceById(forced);
                exercise = JSON.parse(exercise.exercice);
            } else {
                exercise = global.resources.getExercice();
                exercise = JSON.parse(exercise.exercice);
            }
        } catch {
            console.log("No exercise exists. Test mode. Fetching default scenario.");
            await this.fetchedLocalExercise();
            exercise = scenario;
            exercise.map = mapName;
        }

        this.loadScenario(exercise);
        this.scene.start(sceneName);
    }

    /**
     * @description Load the scenario
     * @param {object} scenario 
     */
    loadScenario = (scenario) => {

    }

    /**
     * @description Load the game mode
     */
    loadGameMode = () => {
        let gameMode = global.util.getGamemode();
        // Stop all scenes except the manager
        this.scene.manager.scenes.forEach(scene => {
            if(scene.scene.key !== 'Manager')
            {
                scene.scene.stop();
            }
        });

        switch(gameMode) {
            case global.Gamemode.Evaluate:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Train:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Explore:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
                
            case global.Gamemode.Create:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
        }
    }

    showTutorialInput = (flag) => { 
        try {
            if(flag) {
                // global.util.showTutorialInputs('btnUpload', 'upload_file');
            } else {
                // global.util.hideTutorialInputs('btnUpload', 'upload_file');
            }
        } catch (error) {
            console.log("Error in showTutorialInput: " + error);
        }
    }

}